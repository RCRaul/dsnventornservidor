package cat.ioc.m7.exercici2_eac_2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class catalegServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");   
        
        String styles = "<style>body{\n" +
"    width:90%;\n" +
"    margin:0px auto;\n" +
"    text-align: center;\n" +
"    font-family: sans-serif;\n" +
"    background-size: cover;\n" +
"    background-repeat: no-repeat;\n" +
"    background-attachment: fixed;\n" +
"    background-position: center center;    \n" +
"    background-image: url(\"https://image.freepik.com/foto-gratis/composicion-literatura-espacio-derecha_23-2147690563.jpg\");    \n" +
"}\n" +
"h1{\n" +
"    margin: 2.5em;\n" +
"    font-size:24px;\n" +
"    -webkit-text-fill-color: burlywood;\n" +
"    -webkit-text-stroke: 1px black;\n" +
"}\n" +
".llibres{\n" +
"    display:grid;\n" +
"    grid-template-columns: repeat(4,1fr);\n" +
"    gap: 1em;\n" +
"    margin:0 15%;  \n" +
"}\n" +
"\n" +
".llibres img{\n" +
"    width: 65%;\n" +
"    border-radius: 5px;\n" +
"    border: 2px solid burlywood;\n" +
"}\n" +
".llibres p, strong{\n" +
"    font-size: 12px;\n" +
"    font-family:fantasy;\n" +
"}\n" +
"</style>";
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
             out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet cataleg</title>");
                    out.println(styles);                   
                    out.println("</head>");                    
                    out.println("<body>");                                        
                    out.println("<h1>Cataleg virtual de llibres DAW</h1>");          

                    //VARIABLES 
                    int nombreParametres = 0;
                    String[] nomParametres = {"imatge_","llibre_","preu_"};
                    
                    //Obtenim els paràmetres inicials del descriptor i els emmagatzemem 
                    Enumeration<String> parametresInicials = getServletConfig().getInitParameterNames();
                    
                    //Sumem tots els paràmetres inicials
                    while(parametresInicials.hasMoreElements()){
                        nombreParametres++;
                        parametresInicials.nextElement();
                    }

                    //Dividim el nombre total de paràmetres entre el nombre d'atributs
                    //Així obtenim el nombre total de paràmetres
                    nombreParametres = nombreParametres/nomParametres.length;
                                        
                    //Mostrem el llistat de llibres a la web
                    out.println("<main class='llibres'>");                    
                    int i = 0;            
                    do{
                        int comptador = 0;
                        i++;               
                        out.println("<div>");
                        for(String element : nomParametres ){                  
                            String str = "";
                            
                            switch(comptador){
                                case 0: 
                                        str = "<img src='"+getServletConfig().getInitParameter(element.concat(String.valueOf(i)))+"'/>";
                                       break;
                                case 1:
                                    str = "<p>"+getServletConfig().getInitParameter(element.concat(String.valueOf(i)))+"</p>";
                                    break;
                                case 2:
                                    str = "<strong>"+getServletConfig().getInitParameter(element.concat(String.valueOf(i)))+" €</strong>";
                                    break;
                                default:
                                    break;                                
                            }                          
                            out.println(str);                 
                            comptador ++;
                        }    
                        out.println("</div>");
                    }while(i<nombreParametres);            
                    out.println("</main>");
                    out.println("</body>");
                    out.println("</html>");           
                }
        }    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
