package cat.ioc.m7.exercici3_eac_2;

import javax.ejb.Stateless;

@Stateless
public class usuariBean implements usuariBeanLocal {

    private String nom;
    private String cognoms;    
    private String edat;
    private String email;
    
    //METODES SET    
    @Override
    public void setNom (String nom){
        this.nom = nom;
    }
    @Override
    public void setCognoms (String cognoms){
        this.cognoms = cognoms;
    }
     @Override
    public void setEdat (String edat){
        this.edat = edat;
    }
    @Override
    public void setEmail (String email){
        this.email = email;
    }
    
    //METODES GET
    @Override
    public String getNom() {
     return this.nom;
   }
    @Override
    public String getCognoms (){
        return this.cognoms;
    }
    @Override
    public String getEdat(){
        return this.edat;
    }
    @Override
    public String getEmail(){
        return this.email;
    }
    
}
