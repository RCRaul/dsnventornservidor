package cat.ioc.m7.exercici3_eac_2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

@Stateless
public class restriccionsBean implements restriccionsBeanLocal {
    
    @Override
    public Boolean cadenaNoBuida(String nom){
        return nom.isEmpty();
    } 
    @Override
    public Integer major18(String edat){
        int anyAra = 0;
        int anyNaixement = 0;
        if(!edat.equals("")){
            Date naixement = null;
            try {
                naixement = new SimpleDateFormat("yyyy-MM-DD").parse(edat);
            } catch (ParseException ex) {
                Logger.getLogger(restriccionsBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            anyAra = Calendar.getInstance().get(Calendar.YEAR);
            Calendar cal = Calendar.getInstance();
            cal.setTime(naixement);
            anyNaixement = cal.get(Calendar.YEAR);
        }
        return (anyAra - anyNaixement);
    }

    
}
