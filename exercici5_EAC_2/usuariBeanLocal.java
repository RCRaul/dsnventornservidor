package cat.ioc.m7.exercici3_eac_2;
import javax.ejb.Local;

@Local
public interface usuariBeanLocal {
    
    public void setNom (String nom);  
    public void setCognoms (String cognoms);
    public void setEdat (String edat);
    public void setEmail (String email);
    
    public String getNom();
    public String getCognoms();
    public String getEdat();
    public String getEmail();   
}