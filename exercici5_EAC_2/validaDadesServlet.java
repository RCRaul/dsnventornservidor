package cat.ioc.m7.exercici3_eac_2;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "validaDadesServlet", urlPatterns = {"/validaDadesServlet"})
public class validaDadesServlet extends HttpServlet {
    
    @EJB
    private usuariBeanLocal usuari;    
    @EJB
    private restriccionsBeanLocal validacio;
    
    int edat = 0;
    String email;
    String direccio;
    String proveidorEmail = "@gmail.com";    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=iso-8859-1");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            //Obtenim la data de naixement de l'usuari i obtenim la seva edat
            edat = validacio.major18(request.getParameter("naixement"));

            //Fem la crida al Bean restriccionsBean per comprovar si les dades són correctes
            boolean validaEdat = (edat>=18);
            boolean validaCadena = validacio.cadenaNoBuida(request.getParameter("nom"))
                                    && validacio.cadenaNoBuida(request.getParameter("cognoms"));
            
            //En cas de que les dades siguin correctes
            //Crearem el email a partir de les dades introduides i
            //emmagatzarem les dades a usuariBean
            if(!validaCadena && validaEdat){

                //emmagetzem les dades al Bean                
                usuari.setNom(request.getParameter("nom"));
                usuari.setCognoms(request.getParameter("cognoms"));
                usuari.setEdat(String.valueOf(edat));
                
                //Creem l'email i l'emmagatzem al Bean
                email = usuari.getNom().toLowerCase();
                email = email.concat(usuari.getCognoms().toLowerCase().replace(" ", ""));
                email = email.concat(usuari.getEdat()).concat(proveidorEmail);
                usuari.setEmail(email);
                
                //Desem el Bean usuari a la sessió per poder accedir des de el 
                //fitxer mostraEmail.jsp
                request.getSession().setAttribute("usuari", usuari);                
                request.getRequestDispatcher("mostraEmail.jsp").forward(request, response);

                //Anirem a mostrar l'email
                direccio = "mostraEmail.jsp";
            
            //Si les dades no són correctes, anirem a la pàgina d'error
            }else{
                direccio = "error.html";
            }
            //Redireccionem
            response.sendRedirect(direccio);          
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}