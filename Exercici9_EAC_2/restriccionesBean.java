package cat.ioc.m7.exercici5_eac_2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

@Stateless
 public class restriccionesBean implements restriccionesBeanLocal {

    @Override
    public Boolean longitudNom (String nom){
        return ( !nom.equals("") &&  nom.length() >= 7); 
    } 
    @Override
    public Integer major18 (String edat){
        int anyAra = 0;
        int anyNaixement = 0;
        try {
            if(!edat.equals("")){
                Date naixement = new SimpleDateFormat("yyyy-MM-DD").parse(edat);
                anyAra = Calendar.getInstance().get(Calendar.YEAR);
                Calendar cal = Calendar.getInstance();
                cal.setTime(naixement);
                anyNaixement = cal.get(Calendar.YEAR);                
            }            
        }catch (ParseException ex) {
            Logger.getLogger(restriccionesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (anyAra - anyNaixement); 
    }   
}
