<%@page contentType="text/html" pageEncoding="iso-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" type="text/css" href="portadaCSS.css">
        <!--Accedim al Bean usuari de forma directa-->
        <title>Portada de ${usuari.getNom()}</title>
    </head>
    <body>     
        <main>
            <h1>Benvingut</h1>
            <figure>
                <img alt="img usuario"
                     src="https://pbs.twimg.com/profile_images/1602508327/domingo_carnet_pocoyo_400x400" />
            </figure>
            <ol>
                <!--Accedim a les propietats del bean de forma directa-->
                <li><p>Nom</p>${usuari.getNom()}</li>
                <li><p>Cognom</p>${usuari.getCognoms()}</li>
                <li><p>Email</p>${usuari.getEmail()}</li>
                <li><p>Edat</p>${usuari.getEdat()}</li>
            </ol>            
            <ul>
                <li><a href="index.html">Inici</a></li>                
                <li><a href="tancarSessioServlet">Sortir</a></li>
            </ul>       
        </main>
    </body>
</html>
