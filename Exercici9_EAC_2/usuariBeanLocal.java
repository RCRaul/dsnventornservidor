package cat.ioc.m7.exercici5_eac_2;

import javax.ejb.Local;

@Local
public interface usuariBeanLocal {
    
    public void setNom (String nom);  
    
    public void setCognoms (String cognoms);
    
    public void setEmail (String email);
    
    public void setEdat (String data);
    
    public String getNom();
    
    public String getCognoms ();
    
    public String getEmail ();
    
    public String getEdat ();
    
}
