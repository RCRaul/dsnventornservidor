package cat.ioc.m7.exercici5_eac_2;

import javax.ejb.Stateless;

@Stateless
public class usuariBean implements usuariBeanLocal {

    private String nom;
    private String cognoms;
    private String email;
    private String edat;
    
    //METODES SET    
    @Override
    public void setNom (String nom){
        this.nom = nom;
    }
    @Override
    public void setCognoms (String cognoms){
        this.cognoms = cognoms;
    }
    @Override
    public void setEmail (String email){
        this.email = email;
    }
    @Override
    public void setEdat (String edat){
        this.edat = edat;
    }    
    //METODES GET    
    @Override
    public String getNom() {
     return nom;
   }
    @Override
    public String getCognoms (){
        return cognoms;
    }
    @Override
    public String getEmail (){
        return email;
    }
    @Override
    public String getEdat (){
        return edat;
    }
}