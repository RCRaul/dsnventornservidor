package cat.ioc.m7.exercici5_eac_2;
import javax.ejb.Local;

@Local
public interface restriccionesBeanLocal {
    
    public Boolean longitudNom (String nom); 
    
    public Integer major18 (String edat);    
}
